package page

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/quanzo/gservice/strut"
)

// определяем кодировку по заголовкам http. кодировка должна быть задана в виде строку: charset=cp1251;
func CheckEncodingFromHeaders(httpHeaders *http.Header) (charset, ctype string) {
	var (
		strCharset string = "charset="
	)
	charset = ""
	ctype = ""

	// определимся где брать данные по кодировке и заодно определим тип контента
	if httpHeaders != nil && (*httpHeaders)["Content-Type"] != nil && len((*httpHeaders)["Content-Type"]) > 0 {
		for _, s := range (*httpHeaders)["Content-Type"] {
			partCT := strings.Split(s, ";")
			for _, ps := range partCT {
				charsetStatPos := strings.Index(ps, strCharset)
				mimePos := strings.Index(ps, "/")
				if charsetStatPos != -1 { // кодировка найдена
					strNext := ps[(charsetStatPos + len(strCharset)):]
					c := strut.IndexOfFirst(strNext, []string{";", "\"", " "})
					if c != -1 {
						charset = strings.Trim(strings.ToLower(strNext[0:c]), " ")
					} else {
						charset = strings.Trim(strings.ToLower(strNext), " ")
					}
				} else {
					if mimePos != -1 { // найден тип контента
						ctype = strings.Trim(ps, " ")
					}
				}
			}
		}
	}

	/*
		if charsetSearchSrc == nil { // в http заголовках не найдена информация об кодировке
			charsetSearchSrc = strBody
		}
		c = strings.Index(*charsetSearchSrc, strCharset)
		if c != -1 { // кодировка найдена
			strNext := (*charsetSearchSrc)[(c + len(strCharset)):]
			c = strut.IndexOfFirst(strNext, []string{";", "\"", " "})
			fmt.Println(c)
			if c != -1 {
				charset = strings.Trim(strings.ToLower(strNext[0:c]), " ")
			} else {
				//strDocCharset = strings.Trim(strings.ToLower(strNext), " ")
			}
		}*/
	return
} // end CheckEncoding

func CheckEncodingFromBody(body *string) (charset string) {
	var (
		strCharset string = "charset="
		c          int
	)
	charset = ""
	c = strings.Index(*body, strCharset)
	if c != -1 { // кодировка найдена
		strNext := (*body)[(c + len(strCharset)):]
		c = strut.IndexOfFirst(strNext, []string{";", "\"", " "})
		if c != -1 {
			charset = strings.Trim(strings.ToLower(strNext[0:c]), " ")
		} else {
			//strDocCharset = strings.Trim(strings.ToLower(strNext), " ")
		}
	}
	return
} // end CheckEncodingFromBody

func GetFromHeaders(httpHeaders *http.Header) (lastModified string, expires string, contentLength int64) {
	contentLength = -1
	if httpHeaders != nil {
		if (*httpHeaders)["Last-Modified"] != nil && (*httpHeaders)["Last-Modified"][0] != "" {
			lastModified = strings.Trim((*httpHeaders)["Last-Modified"][0], " ")
		}
		if (*httpHeaders)["Expires"] != nil && (*httpHeaders)["Expires"][0] != "" {
			expires = strings.Trim((*httpHeaders)["Expires"][0], " ")
		}

		if (*httpHeaders)["Content-Length"] != nil && (*httpHeaders)["Content-Length"][0] != "" {
			var int_err error
			contentLength, int_err = strconv.ParseInt((*httpHeaders)["Content-Length"][0], 10, 64)
			if int_err != nil {
				contentLength = -1
			}
		}
	}
	return
} // end GetFromHeaders
