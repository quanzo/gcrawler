package page

import (
	"net/http"
	"time"

	"github.com/quanzo/gservice/status"
	url "github.com/quanzo/gservice/url"
)

type IHttpPageGetter interface {
	Get(u *url.Url) *status.Status    // Загрузить страницу.
	SetRequestHeaders(h *http.Header) // Заголовки запроса.
}

type IStdHttpPage interface {
	GetPageStatus() *status.Status           // код статуса страницы
	GetPageUrl() *url.Url                    // Адрес страницы.
	GetPageBody() string                     // Содержимое страницы.
	GetPageType() string                     // Mime тип страницы.
	GetPageCurrentEnc() string               // Текущая кодировка страницы.
	GetPageEnc() string                      // Кодировка страницы, как она была получена.
	IsRedirect() bool                        // Признак того, что страница на самом деле доступна не по GetPageUrl.
	GetRedirect() *url.Url                   // Реальное место расположения страницы. Если не редирект, то равно nil.
	GetLength() int64                        // Размер.
	GetLastModifiedTime() (time.Time, error) // Время последней модификации страницы.
	GetExpiresTime() (time.Time, error)      // Время, после которого истекает актуальность страницы.
	GetHeaders() *http.Header                // Служебные заголовки страницы.
}

type IHttpPage interface {
	IStdHttpPage
	IHttpPageGetter
}

type IHtmlPage interface {
	GetBodyTag() string     // Содержимое тега body.
	GetHeaderTag() string   // Содержимое тега header.
	GetTitle() string       // Заголовок страницы.
	GetDescription() string // Описание страницы.
	GetKeywords() string    // Ключевые слова страницы.
	GetLink() []*url.Url    // Список ссылок с страницы.
	GetH1() []string        // Заголовки H1
	GetH2() []string        // Заголовки H2
	GetH3() []string        // Заголовки H3
	GetH4() []string        // Заголовки H4
}
