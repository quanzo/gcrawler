package rank

/* расчет веса страниц сайта на основании данных из базы данных
- формат таблиц, как при использовании save
- в таблице page должно быть создано поле rank
*/

import (
	"database/sql"
	//	"log"
	"sync"

	"github.com/quanzo/gservice/status"
)

type RankDb struct {
	IRanker
	lock *sync.Mutex
	db   *sql.DB
}

func (this *RankDb) CalcAll() error {
	if this.lock != nil {
		this.lock.Lock()
		defer this.lock.Unlock()
	}

	if _, err := this.db.Exec("UPDATE page SET pagerank=(SELECT SUM(sourcerank2dest) FROM page2page WHERE page.url=page2page.dest) WHERE page.url IN (SELECT DISTINCT url FROM page)"); err != nil {
		return status.New(0, "Error calc page rank. Error: "+err.Error(), true, false)
	}
	return nil
}

// Расчет rank для всех страниц
/*func (this *RankDb) CalcAll() error {
	if this.lock != nil {
		this.lock.Lock()
		defer this.lock.Unlock()
	}
	var (
		//		err  error
		rows *sql.Rows
	)

	if selTrx, err := this.db.Begin(); err == nil {
		log.Println("Select transaction start")
		if rows, err = selTrx.Query("SELECT dest, SUM(sourcerank2dest) FROM page2page GROUP BY dest"); err == nil {
			defer rows.Close()
			var (
				dest string
				rank float64
			)
			// перебираем данные и заносим их в таблицу страниц
			for rows.Next() {
				rows.Scan(&dest, &rank)
				if updTrx, err := this.db.Begin(); err == nil {
					if _, err := updTrx.Exec("UPDATE page SET pagerank=? WHERE url=?", rank, dest); err == nil {
						updTrx.Commit()
					} else {
						updTrx.Rollback()
						log.Println("Error update. ", err)
					}
				} else {
					log.Println("Error start upd transaction. ", err)
				}
			} // end for
		} else {
			log.Println("Error select query. ", err)
		}

	} else {
		log.Println("Error start select transaction. ", err)
	}
	return nil
}*/

/*func (this *RankDb) CalcAll() error {
	if this.lock != nil {
		this.lock.Lock()
		defer this.lock.Unlock()
	}
	var (
		err  error
		rows *sql.Rows
	)

	// получаем рассчитанный rank для всех страниц на которые есть ссылки
	if rows, err = this.db.Query("SELECT dest, SUM(sourcerank2dest) FROM page2page GROUP BY dest"); err == nil {
		defer rows.Close()
		var (
			dest string
			rank float64
		)
		// перебираем данные и заносим их в таблицу страниц
		for rows.Next() {
			rows.Scan(&dest, &rank)

			if _, err = this.db.Exec("UPDATE page SET pagerank=? WHERE url=?", rank, dest); err != nil {
				log.Println("Error update rank ", dest, rank, " Error: ", err)
			}
		} // end for
	} else {
		return err
	}
	return nil
}*/

func (this *RankDb) GetRank(page string) (float64, error) {
	if this.lock != nil {
		this.lock.Lock()
		defer this.lock.Unlock()
	}
	var (
		rank  float64
		err   error
		prrow *sql.Row
	)
	prrow = this.db.QueryRow("SELECT pagerank FROM page WHERE url=?", page)
	if err = prrow.Scan(&rank); err != nil || rank == 0 {
		prrow = this.db.QueryRow("SELECT SUM(sourcerank2dest) FROM page2page WHERE dest=?", page)
		if err = prrow.Scan(&rank); err != nil {
			return -1, status.New(1, "No rank. Error: "+err.Error(), true, false)
		}
	}
	return rank, nil
	/*var (
		stmt *sql.Stmt
		err  error
	)

	if stmt, err = this.db.Prepare("SELECT SUM(sourcerank2dest) FROM page2page WHERE dest=?"); err == nil {
		defer stmt.Close()
		var (
			row  *sql.Row
			rank float64
		)
		row = stmt.QueryRow(page)
		if err = row.Scan(&rank); err == nil {
			return rank, nil
		} else {
			return -1, status.New(1, "No rank. Error: "+err.Error(), true, false)
		}
	} else {
		return -1, status.New(1, "Error prepare sql statement. Error: "+err.Error(), true, false)
	}*/
}
