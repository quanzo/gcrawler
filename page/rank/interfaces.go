package rank

type IRanker interface {
	GetRank(page string) (float64, error) // Вернуть rank для определенной страницы.
}
