package rank

import (
	"database/sql"
	"sync"
)

func NewRankDb(db *sql.DB) IRanker {
	this := new(RankDb)
	this.db = db
	this.CalcAll()
	this.lock = new(sync.Mutex)
	return this
} // end NewRankDB
