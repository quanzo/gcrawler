package page

import (
	"net/http"
	"time"

	"github.com/quanzo/gservice/status"
	url "github.com/quanzo/gservice/url"
)

type StdPage struct {
	IStdHttpPage

	url              *url.Url // адрес страницы
	redirectUrl      *url.Url // адрес ридеректа
	body             string   // содержимое страницы
	bodyType         string   // тип документа
	bodyEncoding     string   // кодировка документа (исходная)
	currBodyEncoding string   // текущая кодировка
	bodyLength       int64    // размер контента
	bodyLastModified string   // время последней модификации
	bodyExpires      string   // время истечения актуальности

	responseHeaders http.Header // заголовки http ответа

	status *status.Status // статус страницы
} // end type

// сеттеры
func (this *StdPage) SetPageStatus(s *status.Status) *StdPage {
	this.status = s
	return this
}
func (this *StdPage) SetPageUrl(u *url.Url) *StdPage {
	this.url = u
	return this
}
func (this *StdPage) SetPageBody(b string) *StdPage {
	this.body = b
	this.bodyLength = int64(len(b))
	return this
}
func (this *StdPage) SetPageType(t string) *StdPage {
	this.bodyType = t
	return this
}
func (this *StdPage) SetPageEnc(e string) *StdPage {
	this.bodyEncoding = e
	return this
}
func (this *StdPage) SetPageCurrentEnc(enc string) *StdPage {
	this.currBodyEncoding = enc
	return this
}
func (this *StdPage) SetRedirect(u *url.Url) *StdPage {
	this.redirectUrl = u
	return this
}
func (this *StdPage) SetLastModified(t string) *StdPage {
	this.bodyLastModified = t
	return this
}
func (this *StdPage) SetExpires(t string) *StdPage {
	this.bodyExpires = t
	return this
}
func (this *StdPage) SetResponseHeaders(h http.Header) *StdPage {
	this.responseHeaders = h
	return this
}

// геттеры
func (this *StdPage) GetPageStatus() *status.Status {
	return this.status
}
func (this *StdPage) GetPageUrl() *url.Url {
	return this.url
}
func (this *StdPage) GetPageBody() string {
	return this.body
}
func (this *StdPage) GetPageType() string {
	return this.bodyType
}
func (this *StdPage) GetPageEnc() string {
	return this.bodyEncoding
}
func (this *StdPage) GetPageCurrentEnc() string {
	return this.currBodyEncoding
}
func (this *StdPage) IsRedirect() bool {
	return (this.redirectUrl != nil)
}
func (this *StdPage) GetRedirect() *url.Url {
	return this.redirectUrl
}
func (this *StdPage) GetLength() int64 {
	return this.bodyLength
}
func (this *StdPage) GetLastModifiedTime() (time.Time, error) {
	return time.Parse(time.RFC1123, this.bodyLastModified)
}
func (this *StdPage) GetExpiresTime() (time.Time, error) {
	return time.Parse(time.RFC1123, this.bodyExpires)
}
func (this *StdPage) GetHeaders() *http.Header {
	return &this.responseHeaders
}
