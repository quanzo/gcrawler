package page

import (
	"github.com/quanzo/gservice/status"
)

func init() {}

func NewEmptyPage() IHttpPage {
	this := new(HttpPage)
	this.httpPage = new(StdPage)
	this.IStdHttpPage = this.httpPage
	this.reset()
	return IHttpPage(this)
} // end EmptyPage

func NewHtmlPage(p IHttpPage) (IHtmlPage, *status.Status) {
	this := new(PageInfo)
	stat := this.Fill(p)
	if stat != nil {
		if stat.IsError() {
			return nil, stat
		} else {
			return this, stat
		}
	}
	return this, nil
} // end HtmlPage
