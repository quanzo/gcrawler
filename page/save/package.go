package save

import (
	"database/sql"
)

func NewDBSave(DB *sql.DB, modeSaveBody, modeSaveHeaders, isLock bool) *DBSave {
	this := new(DBSave)
	this.db = DB
	this.modeSaveBody = modeSaveBody
	this.modeSaveHeaders = modeSaveHeaders
	this.isLock = isLock
	return this
}
