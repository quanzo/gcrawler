package save

import (
	"github.com/quanzo/gcrawler/page"
	"github.com/quanzo/gservice/status"
)

type IPageSaver interface {
	Save(httpPage page.IHttpPage, htmlPage page.IHtmlPage) *status.Status
	Clear()
}
