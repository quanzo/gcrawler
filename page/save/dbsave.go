package save

/* Сохранение сведений по странице в БД
Ошибки:
	1 - ошибка начала транзакции
	2- ошибка создания 1 таблицы
	3 - ошибка создания второй таблицы
	4 - ошибка коммита
	5 - ошибка rollback
	6 - ошибка подготовки запроса
	7 - ошибка выполнения запроса
	8 - данные не добавлены


*/

import (
	"database/sql"
	"sync"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/quanzo/gcrawler/page"
	"github.com/quanzo/gservice/status"
)

type entry struct {
	url           string
	docdate       string
	lastmodified  string
	expires       string
	external      int
	c404          int
	redirect      string
	contentType   string
	contentLength int64
	httpStatus    int
	title         string
	description   string
	keywords      string
	h1            string
	h2            string
	h3            string
	h4            string
	isReady       int
	body          string
	headers       string
}

type DBSave struct {
	IPageSaver
	db                            *sql.DB
	modeSaveBody, modeSaveHeaders bool
	lock                          sync.Mutex
	isLock                        bool
}

func (this *DBSave) formEntry(httpPage page.IHttpPage, htmlPage page.IHtmlPage) *entry {
	var res entry

	res.body = httpPage.GetPageBody()
	res.contentLength = httpPage.GetLength()
	res.contentType = httpPage.GetPageType()
	res.description = htmlPage.GetDescription()
	res.external = 0
	if httpPage.GetPageUrl().IsExternal() {
		res.external = 1
	}
	res.isReady = 1
	res.keywords = htmlPage.GetKeywords()

	res.url = httpPage.GetPageUrl().String()

	if httpPage.GetPageStatus().GetCode() == 404 {
		res.c404 = 1
	} else {
		res.c404 = 0
	}

	if httpPage.GetRedirect() != nil {
		res.redirect = httpPage.GetRedirect().String()
	}
	for _, val := range htmlPage.GetH1() {
		res.h1 += val + "\r\n"
	}
	for _, val := range htmlPage.GetH2() {
		res.h2 += val + "\r\n"
	}
	for _, val := range htmlPage.GetH3() {
		res.h3 += val + "\r\n"
	}
	for _, val := range htmlPage.GetH4() {
		res.h4 += val + "\r\n"
	}
	if this.modeSaveHeaders {
		respHeaders := httpPage.GetHeaders()
		if respHeaders != nil {
			for key, val := range *respHeaders {
				res.headers += key + ": "
				for _, val2 := range val {
					res.headers += val2 + "\r\n"
				}
			}
		}
	}
	// дата последней модификации
	res.lastmodified = "0000-00-00 00:00:00"
	if tGLM, t_err := httpPage.GetLastModifiedTime(); t_err == nil {
		res.lastmodified = this.formatTime(tGLM)
	}
	// дата истечения срока годности ресурса
	res.expires = "0000-00-00 00:00:00"
	if tExpires, t_err := httpPage.GetExpiresTime(); t_err == nil {
		res.expires = this.formatTime(tExpires)
	}
	return &res
}

func (this *DBSave) Create() (stat *status.Status) {
	if this.isLock {
		this.lock.Lock()
		defer this.lock.Unlock()
	}

	var (
		transDb         *sql.Tx
		errCode         int
		err             error
		SQL, errMessage string
	)

	defer func() {
		if err == nil {
			errCommit := transDb.Commit()
			if errCommit != nil {
				stat = status.New(5, "Ошибка коммита при создании таблиц: "+err.Error(), true, false)
				transDb.Rollback()
			} else {
				stat = nil
			}
		} else {
			stat = status.New(errCode, errMessage+err.Error(), true, false)
			transDb.Rollback()
		}
	}() // end defer

	// создадим таблицы
	transDb, err = this.db.Begin() // стартует транзакция
	if err == nil {                // транзакция начата
		// создаем таблицу `page`
		SQL = "CREATE TABLE IF NOT EXISTS `page` (`url` varchar(255) NOT NULL DEFAULT '', `docdate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `lastmodified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `expires` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `external` tinyint(1) NOT NULL DEFAULT '0', `c404` tinyint(1) NOT NULL DEFAULT '0', `httpStatus` INT NOT NULL DEFAULT '200' , `redirect` varchar(255) NOT NULL DEFAULT '', `contentType` varchar(100) NOT NULL DEFAULT '', `contentLength` int NOT NULL DEFAULT 0,  `title` varchar(255) NOT NULL DEFAULT '', `description` varchar(300) NOT NULL DEFAULT '', `keywords` varchar(300) NOT NULL DEFAULT '', `h1` text NOT NULL DEFAULT '',  `h2` text NOT NULL DEFAULT '', `h3` text NOT NULL DEFAULT '', `h4` text NOT NULL DEFAULT ''"

		if this.modeSaveBody {
			SQL += ", `body` text NOT NULL "
		}
		if this.modeSaveHeaders {
			SQL += ", `headers` text NOT NULL"
		}
		SQL += ", `isReady` tinyint(1) NOT NULL DEFAULT '0', pagerank real NOT NULL DEFAULT '0', PRIMARY KEY (`url`))"

		_, err = transDb.Exec(SQL)
		if err == nil {
			_, err = transDb.Exec("CREATE TABLE IF NOT EXISTS `page2page` (`source` varchar(300) NOT NULL DEFAULT '', `dest` varchar(300) NOT NULL DEFAULT '', `anchor` varchar(2000) NOT NULL DEFAULT '', `level` INT NOT NULL DEFAULT '1', sourcerank2dest real NOT NULL DEFAULT '0')")
			if err != nil { // ошибка создания второй таблицы
				errCode = 3
				errMessage = "Ошибка создания второй таблицы. "
				return
			}
		} else { // ошибка создания первой таблицы
			errCode = 2
			errMessage = "Ошибка создания первой таблицы. "
			return
		}
	} else { // ошибка старта транзакции
		errCode = 1
		errMessage = "Ошибка старта транзакции"
	}
	return
}

func (this *DBSave) getNowTime() string {
	return this.formatTime(time.Now())
}
func (this *DBSave) formatTime(t time.Time) string {
	return t.Format("2006-01-02 15:04:05")
}

func (this *DBSave) Save(httpPage page.IHttpPage, htmlPage page.IHtmlPage) (result *status.Status) {
	if this.isLock {
		this.lock.Lock()
		defer this.lock.Unlock()
	}
	var (
		//h1, h2, h3, h4, redirect, curr_url, headers, SQL string
		SQL string
		//c404, isReady, errCode int
		errCode               int
		stmPage, stmPage2Page *sql.Stmt
		err                   error
		errMessage            string
		sqlRes                sql.Result
		transDb               *sql.Tx
		//		strGLM, strExpires    string
	)

	defer func() {
		if err == nil {
			err = transDb.Commit()
			if err != nil {
				result = status.New(9, "Ошибка коммита в БД: "+err.Error(), true, false)
				transDb.Rollback()
			} else {
				result = nil
			}
		} else {
			transDb.Rollback()
			result = status.New(errCode, errMessage+": "+err.Error(), true, false)
		}
	}()

	transDb, err = this.db.Begin()
	if err != nil {
		errCode = 1
		errMessage = "Ошибка старта транзакции"
		return
	}

	// транзакция начата
	//**************************************************************************
	SQL = "INSERT INTO `page` (url, docdate, lastmodified, expires, external, c404, redirect, contentType, contentLength, httpStatus, title, description, keywords, h1, h2, h3, h4, isReady"
	if this.modeSaveBody {
		SQL += ", body"
	}
	if this.modeSaveHeaders {
		SQL += ", headers"
	}
	SQL += ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?"
	if this.modeSaveBody {
		SQL += ", ?"
	}
	if this.modeSaveHeaders {
		SQL += ", ?"
	}
	SQL += ")"
	stmPage, err = transDb.Prepare(SQL)
	if err != nil {
		errCode = 6
		errMessage = "Ошибка подготовки запроса по занесению данных в таблицу page"
		return
	}
	stmPage2Page, err = transDb.Prepare("INSERT INTO `page2page` (source, dest, anchor, level, sourcerank2dest) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		errCode = 6
		errMessage = "Ошибка подготовки запроса по занесению данных в таблицу page2page"
		return
	}
	//**************************************************************************
	nowTime := this.getNowTime()
	data := this.formEntry(httpPage, htmlPage)

	if !this.modeSaveBody && this.modeSaveHeaders {
		sqlRes, err = stmPage.Exec(data.url, nowTime, data.lastmodified, data.expires, data.external, data.c404, data.redirect, data.contentType, data.contentLength, data.httpStatus, data.title, data.description, data.description, data.h1, data.h2, data.h3, data.h4, data.isReady, data.headers)
	} else {
		sqlRes, err = stmPage.Exec(data.url, nowTime, data.lastmodified, data.expires, data.external, data.c404, data.redirect, data.contentType, data.contentLength, data.httpStatus, data.title, data.description, data.description, data.h1, data.h2, data.h3, data.h4, data.isReady, data.body, data.headers)
	}

	/*curr_url = httpPage.GetPageUrl().String()

	if httpPage.GetPageStatus().GetCode() == 404 {
		c404 = 1
	} else {
		c404 = 0
	}

	if httpPage.GetRedirect() != nil {
		redirect = httpPage.GetRedirect().String()
	}
	for _, val := range htmlPage.GetH1() {
		h1 += val + "\r\n"
	}
	for _, val := range htmlPage.GetH2() {
		h2 += val + "\r\n"
	}
	for _, val := range htmlPage.GetH3() {
		h3 += val + "\r\n"
	}
	for _, val := range htmlPage.GetH4() {
		h4 += val + "\r\n"
	}
	if this.modeSaveHeaders {
		respHeaders := httpPage.GetHeaders()
		if respHeaders != nil {
			for key, val := range *respHeaders {
				headers += key + ": "
				for _, val2 := range val {
					headers += val2 + "\r\n"
				}
			}
		}
	}

	// выполняем запрос по внесению данных по странице в БД в соответствии с настройками по сохранению заголовков и тела страницы
	nowTime := this.getNowTime()
	// дата последней модификации
	strGLM = "0000-00-00 00:00:00"
	if tGLM, t_err := httpPage.GetLastModifiedTime(); t_err == nil {
		strGLM = this.formatTime(tGLM)
	}
	// дата истечения срока годности ресурса
	strExpires = "0000-00-00 00:00:00"
	if tExpires, t_err := httpPage.GetExpiresTime(); t_err == nil {
		strExpires = this.formatTime(tExpires)
	}

	if this.modeSaveBody && !this.modeSaveHeaders {
		sqlRes, err = stmPage.Exec(curr_url, nowTime, strGLM, strExpires, httpPage.GetPageUrl().IsExternal(), c404, redirect, httpPage.GetPageType(), httpPage.GetLength(), httpPage.GetPageStatus().GetCode(), htmlPage.GetTitle(), htmlPage.GetDescription(), htmlPage.GetKeywords(), h1, h2, h3, h4, isReady, httpPage.GetPageBody())
	} else if !this.modeSaveBody && this.modeSaveHeaders {
		sqlRes, err = stmPage.Exec(curr_url, nowTime, strGLM, strExpires, httpPage.GetPageUrl().IsExternal(), c404, redirect, httpPage.GetPageType(), httpPage.GetLength(), httpPage.GetPageStatus().GetCode(), htmlPage.Title, htmlPage.Description, htmlPage.Keywords, h1, h2, h3, h4, isReady, headers)
	} else if this.modeSaveBody && this.modeSaveHeaders {
		sqlRes, err = stmPage.Exec(curr_url, nowTime, strGLM, strExpires, httpPage.GetPageUrl().IsExternal(), c404, redirect, httpPage.GetPageType(), httpPage.GetLength(), httpPage.GetPageStatus().GetCode(), htmlPage.Title, htmlPage.Description, htmlPage.Keywords, h1, h2, h3, h4, isReady, httpPage.GetPageBody(), headers)
	} else {
		sqlRes, err = stmPage.Exec(curr_url, nowTime, strGLM, strExpires, httpPage.GetPageUrl().IsExternal(), c404, redirect, httpPage.GetPageType(), httpPage.GetLength(), httpPage.GetPageStatus().GetCode(), htmlPage.Title, htmlPage.Description, htmlPage.Keywords, h1, h2, h3, h4, isReady)
	}*/
	if err != nil {
		errCode = 7
		errMessage = "Ошибка выполнения запроса по занесению данных в таблицу `page` " + err.Error()
		return
	}

	count, _ := sqlRes.RowsAffected()
	if count > 0 {
		// данные по странице успешно занесены в БД
		// заносим данные по ссылкам в БД
		for _, val := range htmlPage.GetLink() {
			//_, err = stmPage2Page.Exec(curr_url, val.String(), val.GetText(), val.GetLevel(), val.GetRank())
			_, err = stmPage2Page.Exec(data.url, val.String(), val.GetText(), val.GetLevel(), val.GetRank())
			if err != nil {
				errCode = 7
				errMessage = "Ошибка выполнения запроса по занесению данных в таблицу `page2page` " + err.Error()
				return
			}
		}
	} else {
		errCode = 8
		errMessage = "Данные в таблицу `page` не внесены " + err.Error()
		return
	}
	return
}

func (this *DBSave) Clear() {
	this.db.Exec("DELETE FROM `page`")
	this.db.Exec("DELETE FROM `page2page`")
}
