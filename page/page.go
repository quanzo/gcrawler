package page

import (
	"io/ioutil"
	"net/http"
	"strings"
	"sync"

	"github.com/quanzo/gservice/status"
	url "github.com/quanzo/gservice/url"
	charmap "golang.org/x/text/encoding/charmap"
	transform "golang.org/x/text/transform"
)

type HttpPage struct {
	IStdHttpPage
	IHttpPageGetter
	httpPage *StdPage

	requestHeaders  *http.Header // заголовки http для запроса
	responseHeaders http.Header  // заголовки http ответа
	lock            sync.Mutex
}

func (this *HttpPage) reset() *HttpPage {
	this.httpPage.SetPageBody("")
	this.httpPage.SetPageType("")
	this.httpPage.SetPageCurrentEnc("")
	this.httpPage.SetPageEnc("")
	this.httpPage.SetLastModified("")
	this.httpPage.SetExpires("")
	this.httpPage.SetPageUrl(nil)
	this.httpPage.SetPageStatus(nil)
	this.httpPage.SetRedirect(nil)
	this.requestHeaders = nil
	return this
} // end reset

func (this *HttpPage) Get(addr *url.Url) (stat *status.Status) {
	var (
		err          error
		httpClient   *http.Client
		httpRequest  *http.Request
		httpResponse *http.Response
		byteBuff     []byte
	)

	this.lock.Lock()
	defer this.lock.Unlock()

	stat = nil
	this.reset()
	this.httpPage.SetPageUrl(addr)

	// создаем http клиента через которого будут проходить запросы
	httpClient = new(http.Client)
	httpClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		if this.GetRedirect() == nil {
			this.httpPage.SetRedirect(url.New(req.URL.String(), addr))
		} else {
			this.httpPage.SetRedirect(url.New(req.URL.String(), this.httpPage.GetRedirect()))
		}
		return nil
	}

	// создаем запрос на получение страницы
	httpRequest, err = http.NewRequest("GET", addr.String(), nil)
	if err != nil {
		stat = status.New(1, "Ошибка создания http запроса: "+err.Error(), true, false)
		this.httpPage.SetPageStatus(stat)
		return
	} else {
		if this.requestHeaders != nil {
			httpRequest.Header = *this.requestHeaders
		}
	}

	// через клиента httpClient выполняем запрос httpRequest и получаем ответ httpResponse
	httpResponse, err = httpClient.Do(httpRequest)
	if err == nil {
		defer httpResponse.Body.Close()
		this.httpPage.SetResponseHeaders(httpResponse.Header)
		byteBuff, err = ioutil.ReadAll(httpResponse.Body)
		if err != nil {
			stat = status.New(3, "Не удалось получить тело страницы: "+err.Error(), true, false)
			this.httpPage.SetPageStatus(stat)
		} else {
			this.httpPage.SetPageStatus(status.New(httpResponse.StatusCode, "OK", false, false))
			bodyPage := string(byteBuff)

			// определим доп инфу из заголовков http
			lastMod, bodyExp, _ := GetFromHeaders(&this.responseHeaders)
			this.httpPage.SetLastModified(lastMod)
			this.httpPage.SetExpires(bodyExp)

			// определяем кодировку, полученной информации
			bodyEnc, bodyType := CheckEncodingFromHeaders(&this.responseHeaders)

			if bodyEnc == "" { // кодировка документа по заголовкам не определена
				switch bodyType {
				case "text/html":
					{
						bodyEnc = CheckEncodingFromBody(&bodyPage)
						break
					}
				} // end switch
			}

			this.httpPage.SetPageCurrentEnc(bodyEnc)
			this.httpPage.SetPageEnc(bodyEnc)

			// перекодировка страницы в utf8
			if bodyEnc != "" && bodyEnc != "utf8" && bodyEnc != "utf-8" {
				switch bodyType {
				case "text/html":
					{
						// перекодировка в utf8
						var (
							encErr  error                 // ошибки перекодировки
							encoder transform.Transformer = nil
						)
						switch { // выбор порядка преобразования
						case strings.Index(bodyEnc, "1251") != -1:
							{
								encoder = charmap.Windows1251.NewDecoder()
							}
						} // end switch
						if encoder != nil {
							bodyPage, _, encErr = transform.String(charmap.Windows1251.NewDecoder(), bodyPage)
							if encErr != nil {
								stat = status.New(4, "Ошибка преобразования кодировки "+bodyEnc+": "+encErr.Error(), true, false)
							} else {
								this.httpPage.SetPageCurrentEnc("utf8")
							}
						} else {
							stat = status.New(5, "Нет процедуры для преобразования кодировки "+bodyEnc, true, false)
						}
						break
					}
				} // end switch
			}
			this.httpPage.SetPageType(bodyType)
			this.httpPage.SetPageBody(bodyPage)
		}
	} else {
		stat = status.New(2, "Ошибка получения результатов http запроса: "+err.Error(), true, false)
		this.httpPage.SetPageStatus(stat)
	}
	return
} // end Get

// сеттеры
func (this *HttpPage) SetRequestHeaders(h *http.Header) {
	this.requestHeaders = h
}
