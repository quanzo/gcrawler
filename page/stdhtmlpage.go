package page

import (
	url "github.com/quanzo/gservice/url"
)

type StdHtmlPage struct {
	IHtmlPage

	Header      string
	Title       string
	Description string
	Keywords    string
	Body        string
	Link        []*url.Url
	H1          []string
	H2          []string
	H3          []string
	H4          []string
}

// Содержимое тега body.
func (this *StdHtmlPage) GetBodyTag() string {
	return this.Body
}

// Содержимое тега header.
func (this *StdHtmlPage) GetHeaderTag() string {
	return this.Header
}

// Заголовок страницы.
func (this *StdHtmlPage) GetTitle() string {
	return this.Title
}

// Описание страницы.
func (this *StdHtmlPage) GetDescription() string {
	return this.Description
}

// Ключевые слова страницы.
func (this *StdHtmlPage) GetKeywords() string {
	return this.Keywords
}

// Список ссылок с страницы.
func (this *StdHtmlPage) GetLink() []*url.Url {
	return this.Link
}

// Заголовки H1
func (this *StdHtmlPage) GetH1() []string {
	return this.H1
}

// Заголовки H2
func (this *StdHtmlPage) GetH2() []string {
	return this.H2
}

// Заголовки H3
func (this *StdHtmlPage) GetH3() []string {
	return this.H3
}

// Заголовки H4
func (this *StdHtmlPage) GetH4() []string {
	return this.H4
}
