package page

import (
	"strings"
	"sync"

	goquery "github.com/PuerkitoBio/goquery"
	"github.com/quanzo/gservice/status"
	url "github.com/quanzo/gservice/url"
)

type PageInfo struct {
	StdHtmlPage
	IHttpPage

	lock sync.Mutex
}

func (this *PageInfo) reset() *PageInfo {
	this.Title = ""
	this.Description = ""
	this.Keywords = ""
	this.H1 = make([]string, 0, 1)
	this.H2 = make([]string, 0, 3)
	this.H3 = make([]string, 0, 3)
	this.H4 = make([]string, 0, 3)
	this.Link = make([]*url.Url, 0, 10)

	return this
}

/*
func (this *PageInfo) Get(addr *url.Url) (stat *status.Status) {
	this.lock.Lock()
	defer this.lock.Unlock()

	get_stat := this.HttpPage.Get(addr)
	if get_stat == nil { // данные по странице успешно получен
		this.reset()
		if this.GetPageType() == "text/html" { // работаем с html
			// собираем данные
			var (
				gQuery *goquery.Document
				err    error
			)
			gQuery, err = goquery.NewDocumentFromReader(strings.NewReader(this.GetPageBody()))
			if err == nil {
				// заголовок страницы - тег title
				this.Title = gQuery.Find("title").Text()
				// ссылки со страницы
				gQuery.Find("a").Each(func(i int, sub *goquery.Selection) {
					href, exists := sub.Attr("href")
					if exists {
						lnk := url.New(href, addr)
						if lnk != nil { // урл распарсен корректно
							lnk.SetFragment("") // сотрем из url знак # т.к. по факту открывается страница без #. и поэтому в очередь на обработку вносим адрес без #
							lnk.SetText(sub.Text())
							if !lnk.IsSelf() {
								this.Link = append(this.Link, lnk)
							}
						}
					}
				})
				// рассчитаем rank для ссылок
				if addr.GetRank() > 0 {
					var rank float64 = addr.GetRank() / float64(len(this.Link))
					for _, l := range this.Link {
						l.SetRank(rank)
					}
				}
				// заголовки H1 H2 H3 H4
				gQuery.Find("h1").Each(func(i int, sub *goquery.Selection) {
					this.H1 = append(this.H1, sub.Text())
				})
				gQuery.Find("h2").Each(func(i int, sub *goquery.Selection) {
					this.H2 = append(this.H2, sub.Text())
				})
				gQuery.Find("H3").Each(func(i int, sub *goquery.Selection) {
					this.H3 = append(this.H3, sub.Text())
				})
				gQuery.Find("h4").Each(func(i int, sub *goquery.Selection) {
					this.H4 = append(this.H4, sub.Text())
				})
				// метатеги keywords и description
				gQuery.Find("meta[name=\"keywords\"]").Each(func(i int, sub *goquery.Selection) {
					buff, exists := sub.Attr("content")
					if exists {
						this.Keywords = buff
					}
				})
				gQuery.Find("meta[name=\"description\"]").Each(func(i int, sub *goquery.Selection) {
					buff, exists := sub.Attr("content")
					if exists {
						this.Description = buff
					}
				})
			} else {
				this.status = status.New(6, "Ошибка разбора документа: "+err.Error(), true, false)
				return this.status
			}

		} else {
			// не html
		}
	} else {
		return get_stat
	}
	return nil
} // end Get
*/

// Заполнить сведения о HTML странице
func (this *PageInfo) Fill(p IHttpPage) (stat *status.Status) {
	this.lock.Lock()
	defer this.lock.Unlock()

	this.IHttpPage = p
	this.reset()
	if p.GetPageType() == "text/html" && !p.GetPageStatus().IsError() {
		// собираем данные
		var (
			gQuery *goquery.Document
			err    error
		)
		addr := p.GetPageUrl()
		gQuery, err = goquery.NewDocumentFromReader(strings.NewReader(p.GetPageBody()))
		if err == nil {
			// заголовок страницы - тег title
			this.Title = gQuery.Find("title").Text()
			// ссылки со страницы
			gQuery.Find("a").Each(func(i int, sub *goquery.Selection) {
				href, exists := sub.Attr("href")
				if exists {
					lnk := url.New(href, addr)
					if lnk != nil { // урл распарсен корректно
						lnk.SetFragment("") // сотрем из url знак # т.к. по факту открывается страница без #. и поэтому в очередь на обработку вносим адрес без #
						lnk.SetText(sub.Text())
						if !lnk.IsSelf() {
							this.Link = append(this.Link, lnk)
						}
					}
				}
			})
			// рассчитаем rank для ссылок
			if addr.GetRank() > 0 {
				var rank float64 = addr.GetRank() / float64(len(this.Link))
				for _, l := range this.Link {
					l.SetRank(rank)
				}
			}
			// заголовки H1 H2 H3 H4
			gQuery.Find("h1").Each(func(i int, sub *goquery.Selection) {
				this.H1 = append(this.H1, sub.Text())
			})
			gQuery.Find("h2").Each(func(i int, sub *goquery.Selection) {
				this.H2 = append(this.H2, sub.Text())
			})
			gQuery.Find("H3").Each(func(i int, sub *goquery.Selection) {
				this.H3 = append(this.H3, sub.Text())
			})
			gQuery.Find("h4").Each(func(i int, sub *goquery.Selection) {
				this.H4 = append(this.H4, sub.Text())
			})
			// метатеги keywords и description
			gQuery.Find("meta[name=\"keywords\"]").Each(func(i int, sub *goquery.Selection) {
				buff, exists := sub.Attr("content")
				if exists {
					this.Keywords = buff
				}
			})
			gQuery.Find("meta[name=\"description\"]").Each(func(i int, sub *goquery.Selection) {
				buff, exists := sub.Attr("content")
				if exists {
					this.Description = buff
				}
			})
		} else {
			return status.New(6, "Ошибка разбора документа: "+err.Error(), false, true)
		}
	} else {
		return status.New(7, "Не является HTML.", false, true)
	}
	return nil
} // end Fill
