package crawler

import (
	"github.com/quanzo/gcrawler/page"
	save "github.com/quanzo/gcrawler/page/save"
	"github.com/quanzo/gservice/repository"
	"github.com/quanzo/gservice/stack"
	//	"github.com/quanzo/gservice/status"
	"log"
	"net/http"
	"runtime"
	"time"

	url "github.com/quanzo/gservice/url"
)

type Crawler struct {
	startUrl         *url.Url                // точка старта
	funcCheckUrl     func(url *url.Url) bool // функция проверки запрошенного url на разрешение к его обработке. если возвращает true - url попадает в очередь на обработку
	requestHeaders   *http.Header            // http заголовки для выволнения запроса к серверу
	queueLink        *stack.RFifo            // очередь ссылок на обработку
	linkToReady      repository.IRepository  // список уже обработанных страниц
	pageSaver        save.IPageSaver         // интерфейс сохранение данных обработанной страницы
	counter          int                     // счетчик кол-ва обработанных страниц
	errCounter       int                     // кол-во ошибок
	goServiceCounter int                     // счетчик работающих сервисов (горутин)

	logged                  bool          // разрешение вести лог
	modeThreadCount         int           // кол-во потоков для работы
	delayBeforeStartService time.Duration // задержка после запуска горутины
	delayEmptyCycle         time.Duration // задержка при пустом прогоне цикла (кол-во горутин == modeThreadCount и есть ссылки на обработку ИЛИ ждем завершения всех горутин)
	ifStarted               bool          // признак работы паука
}

func (this *Crawler) Start() {
	if !this.ifStarted {
		var (
			rqUrl     *url.Url
			rqUrlHash string
		)
		this.ifStarted = true
		this.counter = 0
		this.errCounter = 0
		defer func() {
			this.ifStarted = false
			if this.logged {
				log.Printf("Обработано ссылок: %d. Ошибок: %d", this.counter, this.errCounter)
			}
		}()
		if this.logged {
			log.Println("Старт crawler. Кол-во потоков: ", this.modeThreadCount)
		}

		for this.queueLink.Len() > 0 || this.goServiceCounter > 0 { // выполняем цикл до тех пор пока в очереди есть есть ссылки или есть работающие горутины
			if this.queueLink.Len() > 0 && (this.modeThreadCount == 0 || this.goServiceCounter < this.modeThreadCount) { // в очереди есть ссылки на обработку и кол-во потоков не превышает настройку (или монопоточный режим)
				rqUrl = nil
				rqUrl = this.queueLink.Pop().(*url.Url) // принимаем ссылку в обработку
				rqUrlHash = string(rqUrl.Hash())
				if !this.linkToReady.Exist(rqUrlHash) { // если ссылки нет в списке обрабатываемых
					this.linkToReady.Set(rqUrlHash, false)
					if this.counter > 0 { // если ссылка в обработке не первая, то надо проверять относится ли ссылка к обрабатываемому сайту. Если ссылка внешняя, то ссылки с нее не должны попадать в очередь на обработку
						if this.modeThreadCount > 0 {
							// многопоточный режим
							go this.service(rqUrl, true)
						} else {
							// однопоточный режим
							this.service(rqUrl, true)
						}
					} else { // обработка самой первой ссылки
						this.service(rqUrl, false)
					}
					this.counter++
					if this.delayBeforeStartService > 0 && this.modeThreadCount > 0 {
						time.Sleep(this.delayBeforeStartService)
					}
				}
			} else {
				if this.delayEmptyCycle > 0 && this.modeThreadCount > 0 {
					time.Sleep(this.delayEmptyCycle)
				}
			}
			runtime.Gosched() // дадим возможность выполнится горутинам
		} // end for
	} // end if not started
} // end func

func (this *Crawler) service(rqUrl *url.Url, checkExternal bool) { // сервис обработки одной страницы
	this.goServiceCounter++
	defer func() {
		this.goServiceCounter--
	}()
	strURL := rqUrl.String()
	if this.logged {
		log.Printf("Обработка %s\r\n", strURL)
		defer func() {
			log.Printf("Обработано %s\r\n", strURL)
		}()
	}

	httpPage := page.NewEmptyPage()
	if this.requestHeaders != nil {
		httpPage.SetRequestHeaders(this.requestHeaders)
	}
	errGet := httpPage.Get(rqUrl)
	if errGet == nil {
		htmlPage, errHtml := page.NewHtmlPage(httpPage)
		if htmlPage != nil {
			// сохранение данных разбора страницы в БД
			errSave := this.pageSaver.Save(httpPage, htmlPage)
			if errSave != nil && this.logged {
				log.Printf("Ошибка сохранения данных: %s %s\r\n", strURL, errSave.Error())
			}
			if ((checkExternal && !rqUrl.IsExternal()) || !checkExternal) && httpPage.GetPageStatus().GetCode() != 404 { // т.к. обрабатывается только внутренняя структура
				// ссылки заносим в this.queueLink, предварительно удалив из списка уже обработанные и/или уже размещенные в очереди
				//pageLink := htmlPage.Link
				for _, v := range htmlPage.GetLink() {
					if !this.linkToReady.Exist(string(v.Hash())) && !this.queueLink.InStack(v) {
						if this.funcCheckUrl != nil && !v.IsExternal() {
							if this.funcCheckUrl(v) {
								this.queueLink.Push(v)
							} else {
								if this.logged {
									log.Printf("Отклонено %s\r\n", v.String())
								}
							}
						} else {
							this.queueLink.Push(v)
						}
					}
				}
			}
		} else {
			if this.logged {
				log.Printf("Ошибка обработки html: %s %s\r\n", strURL, errHtml.Error())
			}
		}
	} else {
		if this.logged {
			log.Printf("Ошибка получения данных: %s %s\r\n", strURL, errGet.Error())
		}
		this.errCounter++
	}
} // end service

// *********************************
// Сеттеры
func (this *Crawler) SetLogged(l bool) *Crawler {
	this.logged = l
	return this
}

func (this *Crawler) SetHeaders(requestHeaders *http.Header) *Crawler {
	this.requestHeaders = requestHeaders
	return this
}

func (this *Crawler) SetThreadCount(n int) *Crawler {
	this.modeThreadCount = n
	return this
}

// задержка после запуска горутины
func (this *Crawler) SetDelayBeforeStartService(n time.Duration) *Crawler {
	this.delayBeforeStartService = n
	return this
}

// задержка при пустом прогоне цикла (кол-во горутин == modeThreadCount и есть ссылки на обработку ИЛИ ждем завершения всех горутин)
func (this *Crawler) SetDelayEmptyCycle(n time.Duration) *Crawler {
	this.delayEmptyCycle = n
	return this
}

func (this *Crawler) SetCheckFunc(f func(url *url.Url) bool) *Crawler {
	this.funcCheckUrl = f
	return this
}

// ***************************
// геттеры
func (this *Crawler) IfStarted() bool {
	return this.ifStarted
}
