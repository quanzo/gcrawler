package crawler

import (
	"log"

	"github.com/quanzo/gcrawler/page/save"
	"github.com/quanzo/gservice/repository"
	"github.com/quanzo/gservice/stack"
	url "github.com/quanzo/gservice/url"
)

func New(startUrl *url.Url, pageSaver save.IPageSaver, logged bool) *Crawler {
	this := new(Crawler)
	this.funcCheckUrl = func(url *url.Url) bool {
		return true
	}
	this.logged = logged
	this.startUrl = startUrl
	this.requestHeaders = nil
	this.pageSaver = pageSaver
	this.modeThreadCount = 9
	this.delayBeforeStartService = 0
	this.delayEmptyCycle = 0

	this.queueLink = stack.NewRFifo()
	this.queueLink.Push(this.startUrl)
	this.linkToReady = repository.New()
	if this.logged {
		log.Printf("Создан crawler. Начало: %s\r\n", startUrl.String())
	}
	return this
} // end New
