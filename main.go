package main

import (
	"database/sql"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	ini "github.com/go-ini/ini"
	"github.com/kardianos/osext"
	_ "github.com/mattn/go-sqlite3"
	"github.com/quanzo/gcrawler/crawler"
	pagerank "github.com/quanzo/gcrawler/page/rank"
	"github.com/quanzo/gcrawler/page/save"
	robotstxt "github.com/quanzo/gcrawler/robotstxt"
	url "github.com/quanzo/gservice/url"
)

func main() {
	/*u1 := url.New("http://site.ru", nil)
	u2 := url.New("dir/", u1)
	u3 := url.New("a/fict/", u1)
	u4 := url.New("../b/fict/", u3)
	fmt.Println(u2, u3, u4)*/

	var (
		workDir                                     string
		err                                         error
		pSiteUrl, pDbFile, pUseRobots, pLocalRobots string
		httpHeaders                                 http.Header
	)
	log.SetOutput(os.Stdout) // все логи
	log.Println("Hello!")
	if workDir, err = osext.ExecutableFolder(); err != nil {
		log.Println("Unable to determine the working directory of the program.")
	} else { // рабочий каталог программы определен - продолжаем
		workDir += string(os.PathSeparator)
		log.Println("Work dir: " + workDir)

		// Конфигурирование из файла config.ini
		if cfg, errCfg := os.Open(workDir + "config.ini"); errCfg == nil {

			if cfgBody, errCfgLoad := ioutil.ReadAll(cfg); errCfgLoad == nil {
				if configIni, errIniCreate := ini.Load(cfgBody); errIniCreate == nil {
					log.Println("File config.ini is used to configure the application.")
					var (
						section    *ini.Section
						errSection error
						sectionKey *ini.Key
					)
					httpHeaders = make(http.Header)
					if section, errSection = configIni.GetSection("HttpHeaders"); errSection == nil {
						for _, sectionKey = range section.Keys() {
							httpHeaders[sectionKey.Name()] = sectionKey.Strings("|")
						}
					}
					/*for p, v := range httpHeaders {
						fmt.Println(p)
						for _, x := range v {
							fmt.Println(x)
						}
					}*/
				} else {
					log.Println("Ignore config.ini. Error convert config.ini: ", errIniCreate.Error())
				}
			} else {
				log.Println("Error file config.ini load: ", errCfgLoad.Error())
			}
			cfg.Close()
		} else {
			log.Println("File config.ini not found: " + errCfg.Error())
		}

		// Параметры командной строки
		flag.StringVar(&pSiteUrl, "s", "", "Адрес сайта для обработки")
		flag.StringVar(&pDbFile, "f", "./sitemap.db", "имя файла в каталоге программы, в который будет размещена база данных")
		flag.StringVar(&pUseRobots, "r", "no", "Использовать robots.txt сайта для определения страниц к индексации. [yes|no]")
		flag.StringVar(&pLocalRobots, "lr", "", "Использовать локальный файл robots.txt, вместо загрузки с сайта. Здесь укажите имя файла.")
		flag.Parse()
		if flag.Parsed() && pSiteUrl != "" {
			var (
				checkUrl func(u *url.Url) bool = func(u *url.Url) bool {
					return true
				}
			)
			baseSiteUrl := url.New(pSiteUrl, nil)
			if baseSiteUrl == nil {
				panic("Не правильно задан URL сайта. Используйте формат http://www.host.ru/")
			}
			SiteUrl := url.New(pSiteUrl, baseSiteUrl).SetRank(1)
			// использрование robots.txt
			var (
				rRobotsTxt io.ReadCloser
				err        error
			)
			if pUseRobots == "yes" {
				log.Println("Будет использован robots.txt с сайта.")
				robotsUrl := SiteUrl.String() + "robots.txt"
				rq, err := http.NewRequest("GET", robotsUrl, nil)
				if err != nil {
					log.Println("Не правильный запрос robots.txt Возможно не корректный URL: " + robotsUrl + " Ошибка: " + err.Error())
				} else {
					if httpHeaders != nil {
						rq.Header = httpHeaders
					}
					httpClient := new(http.Client)
					/*здесь можно вставить настройки клиента*/
					robotsResp, err := httpClient.Do(rq)
					if err != nil {
						log.Println("Ошибка получения robots.txt с сайта. URL: " + robotsUrl + " Error: " + err.Error())
					} else {
						if robotsResp.StatusCode != 200 {
							log.Println("Ошибка получения robots.txt с сайта. URL: " + robotsUrl + " Http код ответа не равен 200. Код ответа: " + robotsResp.Status)
						} else {
							rRobotsTxt = robotsResp.Body
						}
					}
				}
			} else {
				if pLocalRobots != "" {
					if rRobotsTxt, err = os.Open(pLocalRobots); err != nil {
						log.Println("Не найден robots.txt в папке программы.")
					}
				}
			}
			if rRobotsTxt == nil {
				log.Println("Обработка robots.txt с сайта будет игнорирована.")
			} else {
				var (
					buffer []byte
				)
				buffer, err = ioutil.ReadAll(rRobotsTxt)
				rRobotsTxt.Close()
				if err == nil {
					robots := robotstxt.New(string(buffer))
					checkUrl = func(u *url.Url) bool {
						return robots.Check("*", u)
					}
					log.Println("Файл robots.txt успешно обработан и будет использован.")
					/*robots, err := robotstxt.FromBytes(buffer)
					if err == nil {
						groupRules := robots.FindGroup("*")
						checkUrl = func(u *url.Url) bool {
							/*var url2test string
							if u.RawQuery != "" {
								url2test = u.Path + "?" + u.RawQuery
							} else {
								url2test = u.Path
							}
							log.Println(url2test, groupRules.Test(url2test))
							return groupRules.Test(url2test)/
							return groupRules.Test(u.String())
						}
						log.Println("Файл robots.txt успешно обработан и будет использован.")
					} else {
						log.Println("Ошибка парсинга содержимого robots.txt. Error: " + err.Error())
					}*/
				} else {
					log.Println("Ошибка чтения robots.txt. Error: " + err.Error())
				}
			}
			// end robots.txt
			// подготовка к запуску
			db, err := sql.Open("sqlite3", pDbFile) // имя файла
			if err != nil {
				panic(err.Error())
			} else {
				defer db.Close()
				saver := save.NewDBSave(db, true, true, true)
				statSave := saver.Create()
				if statSave == nil {
					log.Println("Crawler start")
					mCrawler := crawler.New(SiteUrl, saver, true)
					mCrawler.SetCheckFunc(checkUrl)
					if httpHeaders != nil {
						mCrawler.SetHeaders(&httpHeaders)
					}
					mCrawler.Start()
					// Page rank calc.
					_ = pagerank.NewRankDb(db)
				} else {
					panic("База данных не готова. Структура не создана. Ошибка:" + statSave.String())
				}
			}
		} else {
			fmt.Println("Не задан обязательный параметр s (имя сайта)")
			fmt.Println("\r\nПараметры коммандной строки:")
			flag.PrintDefaults()
		}
	}
	log.Println("Bye! Bye!")
} // end main
