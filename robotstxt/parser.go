package robotstxt

import (
	"sort"
	"strings"

	"github.com/quanzo/bufferstring"
	url "github.com/quanzo/gservice/url"
)

type RobotsFile struct {
	data        Sections
	sectionName string
}

func (this *RobotsFile) appendVal(sect, param, val string, line int) {
	if sect != "" && param != "" {
		var ok bool
		if _, ok = this.data[sect]; !ok {
			this.data[sect] = newParams()
		}
		this.data[sect].Append(Param{Name: param, Value: val, Line: line})
	}
}

func (this *RobotsFile) parse(text string) {
	source := bufferstring.NewFromString(text, 0)
	buff := bufferstring.New(50, 10)

	var (
		param_name, param_val, section string = "", "", "*"
		check_value, isComment         bool   = false, false
		lineCounter                    int    = 0
	)

	source.Walk(0, source.Length(), func(i int, v *rune) {
		if *v == ':' && !check_value {
			param_name = strings.Trim(strings.ToLower(buff.String()), " ")
			param_val = ""
			buff.Empty()
			check_value = true
		} else {
			if *v == '\n' {
				if check_value {
					param_val = strings.Trim(buff.String(), " ")
				} else {
					param_name = strings.Trim(strings.ToLower(buff.String()), " ")
					param_val = ""
				}
				if param_name == this.sectionName {
					section = param_val
					if section == "" {
						section = "*"
					}
				} else {
					this.appendVal(section, param_name, param_val, lineCounter)
				}
				param_name, param_val = "", ""
				buff.Empty()
				check_value = false
				isComment = false
				lineCounter++
			} else {
				if *v == '#' {
					isComment = true
				}
				if *v != '\r' && !isComment {
					buff.AppendRune(*v)
				}
			}
		}
	})
	for _, params := range this.data {
		sort.Sort(params)
	}
} // end parse

func (this *RobotsFile) Check(sect string, u *url.Url) bool {
	if params, ok := this.data[sect]; ok {
		if param := params.Check(u); param != nil {
			return param.Name == "allow"
		}
	}
	return true
}
