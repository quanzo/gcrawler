package robotstxt

func New(txt string) *RobotsFile {
	this := new(RobotsFile)
	this.sectionName = "user-agent"
	this.data = make(Sections)
	this.parse(txt)
	return this
}
