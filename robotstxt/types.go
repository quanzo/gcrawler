package robotstxt

import (
	"strings"

	"github.com/quanzo/gservice/buffer"
	"github.com/quanzo/bufferstring"
	url "github.com/quanzo/gservice/url"
)

type Param struct {
	Name  string // имя параметра
	Value string // значение
	Line  int    // номер строки в файле
}
type Sections map[string]*Params

type Params struct {
	*buffer.Buffer
}

func newParams() *Params {
	this := new(Params)
	this.Buffer = buffer.NewEmpty(4, 2)
	return this
}

func (this *Params) Len() int {
	return this.Length()
}

func (this *Params) Less(i, j int) bool {
	if i >= 0 && j >= 0 && i < this.Length() && j < this.Length() {
		param_1, _ := this.One(i)
		param_2, _ := this.One(j)
		if strings.Compare((param_1.(Param)).Value, (param_2.(Param)).Value) < 0 {
			return true
		}
	}
	return false
}
func (this *Params) Check(u *url.Url) *Param {
	relUrl := bufferstring.NewFromString(u.GetPath(), 10)
	uQuery := u.GetQuery().Encode()
	if uQuery != "" {
		relUrl.AppendString("?", uQuery)
	}

	var (
		err              error
		i, pos           int
		unknown          interface{}
		param, res_param Param
		i_find           int = -1
	)
	maskCount := this.Length()
	for i = 0; i < maskCount; i++ {
		if unknown, err = this.One(i); err == nil {
			param = unknown.(Param)
			pos, _ = relUrl.FindMaskAdv(param.Value, 0, '*', 0)
			if pos == 0 && (param.Name == "allow" || param.Name == "disallow") {
				i_find = i
				res_param = param
			}
		}
	}
	if i_find == -1 {
		return nil
	} else {
		return &res_param
	}
}
