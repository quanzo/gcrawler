package robotstxt

import (
	"testing"

	url "github.com/quanzo/gservice/url"
)

const TXT = `User-Agent: *
# this comment
Disallow: */index.php # this comment
Disallow: /bitrix/ # this comment
Disallow: /*show_include_exec_time=
Disallow: /*show_page_exec_time=
Allow: /bitrix/templates/
Disallow: /*show_sql_stat=
Disallow: /*bitrix_include_areas=
Disallow: /*clear_cache=
Disallow: /*clear_cache_session=
    
Disallow: /bitrix/templates/*?noindex*
Disallow: /*BACK_URL=
Disallow: /*back_url_admin=
Disallow: /*?utm_source=
Allow: /bitrix/js/

Allow: /bitrix/panel/
User-Agent: Yandex
Disallow: /*ORDER_BY
Disallow: /*PAGEN
Disallow: /*?print=
Disallow: /*&print=
Disallow: /*print_course=
Disallow: /*?action=
Disallow: /*&action=
Disallow: /*register=
Disallow: /*forgot_password=
Disallow: /*change_password=
Disallow: /*login=
Disallow: /*logout=
Disallow: /*auth=
Allow: /bitrix/components/
Allow: /bitrix/cache/
      
   
`

type TestUrl struct {
	robotstxt string
	sect      string
	u         *url.Url
	res       bool
}

var tUrl []TestUrl = []TestUrl{
	TestUrl{TXT, "*", url.New("/index.php", nil), false},
	TestUrl{TXT, "*", url.New("http://yandex.ru/index.php", nil), false},
	TestUrl{TXT, "*", url.New("http://yandex.ru/bitrix/index.php", nil), false},
	TestUrl{TXT, "*", url.New("http://yandex.ru/bitrix/templates/index.php", nil), true},
	TestUrl{TXT, "*", url.New("http://yandex.ru/bitrix/templates/detail.php?noindex", nil), false},
}

func TestParse(t *testing.T) {
	txt := TXT
	p := New(txt)
	for i, s := range p.data {
		for b_i := 0; b_i < s.Length(); b_i++ {
			v, _ := s.One(b_i)
			t.Log(i, ">", v.(Param))
		}
	}
}

func TestUrls(t *testing.T) {
	p := New(TXT)
	for i, ts := range tUrl {
		if p.Check(ts.sect, ts.u) != ts.res {
			t.Error("Error #", i, " ", p.Check(ts.sect, ts.u))
		}
	}
}
